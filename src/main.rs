use ansi_term::Colour::{Fixed, Green, Red};
use chrono::prelude::*;
use chrono::Duration;
use std::collections::BTreeMap; // We use BTreeMap instead of HashMap to keep the ordering of the JSON file
use std::fs;
use std::fs::File;
use std::io;
use std::io::Write;
use std::num::ParseIntError;
use std::path::Path;

fn get_days_for_month(month: u32, year: i32) -> Vec<NaiveDate> {
    let mut days = Vec::<NaiveDate>::new();

    for i in 1..31 {
        let date = NaiveDate::from_ymd_opt(year, month, i);

        if date.is_some() {
            days.push(date.unwrap());
        } else {
            break;
        }
    }

    days
}

fn get_days_for_week(week: u32, year: i32) -> Vec<NaiveDate> {
    let mut days = Vec::<NaiveDate>::with_capacity(7);
    let mut day = NaiveDate::from_isoywd_opt(year, week, Weekday::Mon).expect(&format!(
        "ywd combination should be valid, year: {} week: {}",
        year, week
    ));
    days.push(day);

    for _i in 1..7 {
        day = day + Duration::days(1);
        days.push(day);
    }

    days
}

fn format_duration(duration: Duration) -> String {
    format!(
        "{:02}:{:02}",
        duration.num_seconds() / 3600,
        (duration.num_seconds() % 3600) / 60
    )
}

fn format_duration_diff(real: Duration, target: Duration) -> String {
    let sign = if real < target { "-" } else { "+" };
    let diff = if real < target {
        target - real
    } else {
        real - target
    };
    format!("{}{}", sign, format_duration(diff))
}

fn print_badges_for_month(month: u32, year: i32, data: &BTreeMap<String, Vec<String>>) {
    let mut month_total = Duration::zero();

    for day in get_days_for_month(month, year) {
        let day_badges = match data.get(&day.format("%Y%m%d").to_string()) {
            Some(badges) => badges.to_owned(),
            None => continue,
        };

        let day_duration = get_duration_for_day(&day_badges);
        month_total = month_total + day_duration;
        print_badges_for_day(day, data);
    }

    if month_total.is_zero() {
        return;
    }

    println!("\nTotal mois : {}", format_duration(month_total));
}

fn get_week_total_target(current_week: bool) -> Duration {
    let weekday = if current_week {
        Local::now().weekday()
    } else {
        Weekday::Fri
    };
    let hours = match weekday {
        Weekday::Mon => 8,
        Weekday::Tue => 2 * 8,
        Weekday::Wed => 3 * 8,
        Weekday::Thu => 4 * 8,
        Weekday::Fri | Weekday::Sat | Weekday::Sun => 4 * 8 + 7,
    };
    Duration::hours(hours)
}

fn print_badges_for_week(
    week: u32,
    year: i32,
    data: &BTreeMap<String, Vec<String>>,
    current_week: bool,
) {
    let mut week_total = Duration::zero();

    for day in get_days_for_week(week, year) {
        let day_badges = match data.get(&day.format("%Y%m%d").to_string()) {
            Some(badges) => badges.to_owned(),
            None => continue,
        };

        let day_duration = get_duration_for_day(&day_badges);
        week_total = week_total + day_duration;
        print_badges_for_day(day, data);
    }

    if week_total.is_zero() {
        return;
    }

    let week_total_target = get_week_total_target(current_week);

    if week_total < Duration::hours(39) {
        println!(
            "\nTotal semaine : {} {}",
            Red.paint(format_duration(week_total)),
            Fixed(8).paint(format_duration_diff(week_total, week_total_target))
        );
    } else {
        println!(
            "\nTotal semaine : {} {}",
            Green.paint(format_duration(week_total)),
            Fixed(8).paint(format_duration_diff(week_total, week_total_target))
        );
    }
}

fn print_badges_for_day(date: NaiveDate, data: &BTreeMap<String, Vec<String>>) {
    let badges = get_badges_for_day(&date, data);

    if badges.is_empty() {
        return;
    }

    let total_time = get_duration_for_day(&badges);

    let target_time = if date.weekday() == Weekday::Fri {
        Duration::hours(7)
    } else {
        Duration::hours(8)
    };

    let total_str = if badges.len() > 3 {
        if total_time < target_time {
            format!(
                "{} {}",
                Red.paint(format_duration(total_time)),
                Fixed(8).paint(format_duration_diff(total_time, target_time))
            )
        } else {
            format!(
                "{} {}",
                Green.paint(format_duration(total_time)),
                Fixed(8).paint(format_duration_diff(total_time, target_time))
            )
        }
    } else {
        format!("{}", Fixed(8).paint(format_duration(total_time)))
    };

    println!(
        "{} | {} | {}",
        date.format("%d/%m/%Y"),
        badges.join(" "),
        total_str
    );
}

fn get_badges_for_day(date: &NaiveDate, data: &BTreeMap<String, Vec<String>>) -> Vec<String> {
    match data.get(&date.format("%Y%m%d").to_string()) {
        Some(badges) => badges.to_owned(),
        None => Vec::<String>::new(),
    }
}

fn get_duration_for_day(badges: &Vec<String>) -> Duration {
    let mut total_time = Duration::zero();

    for i in (0..badges.len()).step_by(2) {
        // if we have an uneven count of badges we can't compute a duration
        if i + 1 >= badges.len() {
            break;
        }

        let start = NaiveTime::parse_from_str(badges[i].as_ref(), "%H:%M");
        if start.is_err() {
            exit_with_error(
                format!("Error parsing time: {}", start.unwrap_err()).as_ref(),
                2,
            );
        }
        let start = start.unwrap();

        let end = NaiveTime::parse_from_str(badges[i + 1].as_ref(), "%H:%M");
        if end.is_err() {
            exit_with_error(
                format!("Error parsing time: {}", end.unwrap_err()).as_ref(),
                2,
            );
        }
        let end = end.unwrap();

        total_time = total_time + (end - start);
    }

    total_time
}

fn add_badge(storage_path: &str) -> BTreeMap<String, Vec<String>> {
    let mut data = load_data_or_default(&storage_path);
    let today = Local::now().format("%Y%m%d").to_string();
    let now = Local::now().format("%H:%M").to_string();
    match data.get_mut(&today) {
        Some(badges) => {
            badges.push(now);
        }
        None => {
            data.insert(today, vec![now]);
        }
    }
    data
}

fn remove_badge(storage_path: &str) -> BTreeMap<String, Vec<String>> {
    let mut data = load_data(&storage_path);
    if let Some((_, badges)) = data.iter_mut().next_back() {
        badges.remove(badges.len() - 1);
    };
    data
}

fn load_data_or_default(filename: &str) -> BTreeMap<String, Vec<String>> {
    let contents = fs::read_to_string(filename).unwrap_or("{}".to_string());
    serde_json::from_str::<BTreeMap<String, Vec<String>>>(&contents).unwrap_or_default()
}

fn load_data(filename: &str) -> BTreeMap<String, Vec<String>> {
    if !Path::new(filename).exists() {
        println!("file '{}' not found", filename);
        std::process::exit(1);
    }

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    let badges: BTreeMap<String, Vec<String>> = match serde_json::from_str(&contents) {
        Ok(badges) => badges,
        Err(err) => {
            eprintln!("{}", err);
            std::process::exit(1);
        }
    };
    badges
}

fn write_data(storage_path: &str, data: BTreeMap<String, Vec<String>>) {
    let f = File::create(storage_path);
    if f.is_err() {
        exit_with_error("cannot open file for write", 32);
    }
    let f = f.unwrap();
    match serde_json::to_writer_pretty(f, &data) {
        Err(err) => {
            eprintln!("{}", err);
            std::process::exit(1);
        }
        _ => {}
    };
}

fn get_storage_path(args: &mut Vec<String>) -> String {
    let mut arg_index: Option<usize> = None;

    for i in 0..args.len() {
        if args[i] == "-f" {
            arg_index = Some(i);
            break;
        }
    }

    if let Some(index) = arg_index {
        args.remove(index);
        if index < args.len() {
            return args.remove(index);
        }
    }

    match dirs::home_dir() {
        Some(home) => format!("{}/ownCloud/badges.json", home.to_string_lossy()),
        None => String::from("badges.json"),
    }
}

fn exit_with_error(error: &str, exit_code: i32) {
    eprintln!("{}", error);
    std::process::exit(exit_code);
}

fn parse_dry_run(args: &mut Vec<String>) -> bool {
    let mut result = false;
    let mut dry_run_arg_index = 0;

    for i in 0..args.len() {
        if args[i] == "-d" {
            result = true;
            dry_run_arg_index = i;
            break;
        }
    }

    if result {
        args.remove(dry_run_arg_index);
    }

    result
}

static DATE_FORMATS: [&str; 6] = [
    "%Y%m%d", "%d%m%Y", "%Y/%m/%d", "%d/%m/%Y", "%Y-%m-%d", "%d-%m-%Y",
];

fn try_parse_date_from_str(date_str: &str) -> Result<NaiveDate, &str> {
    for format in &DATE_FORMATS {
        let result = NaiveDate::parse_from_str(date_str, format);
        if result.is_ok() {
            return Ok(result.unwrap());
        }
    }

    Err("Unknown date format")
}

fn get_year_or_error() -> Result<i32, ParseIntError> {
    let mut input = String::new();
    print!("Année : ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut input).unwrap();
    input.trim().parse::<i32>()
}

fn main() {
    let mut args: Vec<String> = std::env::args().collect();
    let dry_run = parse_dry_run(&mut args);
    let storage_path = get_storage_path(&mut args);

    if args.len() <= 1 {
        let new_data = add_badge(&storage_path);
        print_badges_for_day(Local::now().date_naive(), &new_data);
        if !dry_run {
            write_data(&storage_path, new_data);
        }
        std::process::exit(0);
    }

    match args[1].as_ref() {
        "show" => {
            if args.len() < 3 {
                // no date given, show current day
                let data = if dry_run {
                    add_badge(&storage_path)
                } else {
                    load_data(&storage_path)
                };
                print_badges_for_day(Local::now().date_naive(), &data);
                return;
            }

            match args[2].as_ref() {
                "week" => {
                    let current_week = args.len() < 4;
                    let week = if args.len() < 4 {
                        Local::now().iso_week().week()
                    } else {
                        match args[3].parse::<u32>() {
                            Ok(week) => week,
                            Err(err) => {
                                eprintln!("{}", err);
                                eprintln!("Please enter a valid week");
                                std::process::exit(1);
                            }
                        }
                    };

                    let year = if args.len() < 4 {
                        Ok(Local::now().year())
                    } else {
                        get_year_or_error()
                    };
                    if year.is_err() {
                        eprintln!("{}", year.unwrap_err());
                        std::process::exit(1);
                    }

                    let data = if dry_run {
                        add_badge(&storage_path)
                    } else {
                        load_data(&storage_path)
                    };
                    print_badges_for_week(week, year.unwrap(), &data, current_week);
                }
                "month" => {
                    let month = if args.len() < 4 {
                        Local::now().month()
                    } else {
                        match args[3].parse::<u32>() {
                            Ok(month) => month,
                            Err(err) => {
                                eprintln!("{}", err);
                                eprintln!("Please enter a valid month");
                                std::process::exit(1);
                            }
                        }
                    };

                    let year = if args.len() < 4 {
                        Ok(Local::now().year())
                    } else {
                        get_year_or_error()
                    };
                    if year.is_err() {
                        eprintln!("{}", year.unwrap_err());
                        std::process::exit(1);
                    }

                    print_badges_for_month(month, year.unwrap(), &load_data(&storage_path));
                }
                _ => {
                    if args[2].len() == 8 || args[2].len() == 10 {
                        let day_result = try_parse_date_from_str(args[2].as_ref());
                        if day_result.is_err() {
                            exit_with_error(day_result.unwrap_err(), 2)
                        }
                        let day = day_result.unwrap();
                        print_badges_for_day(day, &load_data(&storage_path));
                    } else {
                        exit_with_error(format!("Unknown date format `{}`", args[2]).as_ref(), 1);
                    }
                }
            }
        }
        "-1" => {
            let new_data = remove_badge(&storage_path);
            print_badges_for_day(Local::now().date_naive(), &new_data);
            if !dry_run {
                write_data(&storage_path, new_data);
            }
            std::process::exit(0);
        }
        _ => {
            exit_with_error(format!("Unknown command `{}`", args[1]).as_ref(), 1);
        }
    }
}
